class CreateCustomers < ActiveRecord::Migration
  def change
    create_table :customers do |t|
      t.string :name
      t.string :email #, default:example@pippo.it
      t.string :phone
      t.string :address

      t.timestamps null: false
    end
  end
end
